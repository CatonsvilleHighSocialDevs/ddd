#!/usr/bin/python3.7
import discord
from jLib import time_log  # jLib = Josh's Library just some functions I like to use cross-projects, is only time_log
# for now though
import re  # Regex
import os
import random
import config  # Config contains some id information and settings
import asyncio
import shelve

from contextlib import contextmanager  # Added to do timeout on functions [DEPRECIATED]
import threading
import _thread

from asteval import Interpreter  # Handles user input mathematical operations

global token
token = ""
global quotes
quotes ={}





def addquote(msgin):
    global quotes
    quotes[str(msgin.author.id)] = msgin.content
    with shelve.open("storage.db") as db:
        db["quotes"] = quotes

class TimeoutException(Exception):  # Timeout stuff [DEPRECIATED] basically it doesn't work well with the events
    def __init__(self, exception_message=''):
        self.exception_message = exception_message


@contextmanager
def time_limit(seconds, message=''):  # Timeout stuff [DEPRECIATED]
    timer = threading.Timer(seconds, lambda: _thread.interrupt_main())
    timer.start()
    try:
        yield
    except KeyboardInterrupt:
        raise TimeoutException("Timed out for operation {}".format(message))
    finally:
        timer.cancel()


quotesRegex = re.compile(".*⬆.*⬇.*")
classRegex = re.compile('!(f|j|so|se)')
# characters to role ids
communismRegex = re.compile(r"(?i)\b((ours?)|(everyone))\b")

statusMessages = open("statuses.txt", "r").read().split("\n")

communismEmojiReaction = config.communismEmojiReaction


def owoify(text):
    last = 0
    newstring = ""
    for i in text:
        if (i == " " and random.randint(1,5) == 1 and last > 20):
            newstring += " owo "
            last = 0
        else:
            newstring += i
            last+=1
    return newstring


time_log("startup", "Libraries loaded")

DDD = discord.Client()

with shelve.open("storage.db") as db: # Putting this here is kinda hacky, if we could try to fit it into a function that would be ideal
    if not "quotes" in db:
        db["quotes"] = {}
    if not "token" in db:
        db["token"] = "example"
    dbDefault = db["token"] == "example" or db["token"] == ""
    configDefault = config.client_token == "example" or config.client_token == ""
    if (db["token"] != config.client_token or (dbDefault and configDefault)):
        if configDefault and not dbDefault: # If the token in the config is the default, ignore it
            time_log("startup","Using token in database")
        elif not configDefault: # If the token in the config isn't the default, store it
            time_log("startup","Using token in config")
            db["token"] = config.client_token
        elif dbDefault and configDefault : # If both are the default, prompt
            db["token"] = input("Please input bot token: ")
    else:
        time_log("startup","Tokens match")
    token = db["token"]
    quotes = db["quotes"]

async def start_up():  # Things that should be done one time once the bot has started
    global quotes
    with shelve.open("storage.db") as db:
        quotes = db["quotes"];
    await DDD.wait_until_ready()
    time_log("startup", "Bot is ready")
    async for message in DDD.get_channel(config.quotesChannelId).history(limit=50):
        if quotesRegex.match(str(message.reactions)):
            k = "ignored"
        else:
            await message.add_reaction("⬆")
            await message.add_reaction("⬇")
            time_log("runtime", "Added old reactions")


async def status_update():
    await DDD.wait_until_ready()
    while len(statusMessages) > 0:
        try:
            await DDD.change_presence(activity=discord.Game(random.choice(statusMessages)))
        except:  # The except pass here is bad, but the game activity isn't a vital function
            pass
        await asyncio.sleep(config.statusUpdatePeriod)


@DDD.event
async def on_member_join(member):
    if config.classFlairingEnabled:
        classFlairingChannel = DDD.get_channel(config.classFlairingChannelId)
        await classFlairingChannel.send("Welcome {} to the Catonsville High School discord server! "
                                        "First you need a class role. "
                                        "Type '!f' if you are a freshman, '!so' if you're a sophomore, "
                                        "'!j' if you're a junior, or '!se' if you're a senior. " "If you have an issue or "
                                        "aren't any of the above, please @Moderators. Don't forget to read #student_handboo"
                                        "k!".format(member.mention))


@DDD.event
async def on_message(message):
    if message.author is not DDD.user:
        global communismEmojiReaction
        if random.randint(1, 10000) == 1:
            await message.channel.send("You are now breathing manually.")
        if message.content == '!ping':
            await message.channel.send("Pong! " + str(DDD.latency)[:5] + "ms")
        elif classRegex.match(message.content) and message.channel.id == config.classFlairingChannelId:  # Check if the
            # message is a role request and it's in the class fairing channel
            if message.content == "!f":
                await message.author.add_roles(message.guild.get_role(config.freshmanRoleId))
            elif message.content == "!j":
                await message.author.add_roles(message.guild.get_role(config.juniorRoleId))
            elif message.content == "!so":
                await message.author.add_roles(message.guild.get_role(config.sophomoreRoleId))
            elif message.content == "!se":
                await message.author.add_roles(message.guild.get_role(config.seniorRoleId))
            time_log("runtime", "Gave role")
        elif message.channel.id == config.quotesChannelId and config.quotesChannelEnabled:  # This section of the code
            # adds reactions for the #Quotes channel
            await message.add_reaction("⬆")
            await message.add_reaction("⬇")
        elif message.content.startswith("!owoify"):
            async with message.channel.typing():
                if len(message.content.strip().split()) > 1:  # Test to see if the message is just "!owoify" or
                    # "!owoify <text>"
                    text = message.content[8:]  # Selects all text after !owoify
                    await message.channel.send(owoify(text))
                else:
                    past_messages = await message.channel.history(limit=2).flatten()  # Retrieves the message sent before
                    # lazily
                    if past_messages[1].content != "":
                        await message.channel.send(owoify(past_messages[1].content))
        elif message.content in ("!help", "!h"):
            help_text = open("help_message.txt", "r").read()
            await message.channel.send(help_text)
        elif message.content.startswith("!communismToggle"):
            communismEmojiReaction = not communismEmojiReaction
            await message.channel.send("Toggled communism to " + str(communismEmojiReaction))
        elif (re.search(r"!solve ([^`\"A-Za-z]+)", message.content.strip())):  # Matches equation in ```!solve or !solve
            match = re.search(r"!solve ([^`\"A-Za-z]+)", message.content.strip())
            to_solve = match.group(1)
            if to_solve != '':
                aeval = Interpreter()
                try:
                    output = str(aeval(to_solve))
                except OverflowError:
                    output = "Overflow Error"
                except RuntimeError:
                    output = "Runtime Error, Max exponent is 10000"
                if output.strip() == "":
                    output = "No output"
                await message.channel.send(output)
            else:
                await message.channel.send("No input found.")
        elif (message.content.startswith("!quote") and config.quotesEnabled):
            quote = ""
            auth = message.author
            send = False
            if (message.content == "!quote"):
                send = True
                if str(message.author.id) not in quotes:
                    await message.channel.send("There's no quote for you!")
                    send = False
                else:
                    time_log("quotes","Found quote!")
                    quote = quotes[str(message.author.id)]
            elif (message.content != "!quote" and message.mentions):
                send = True
                auth = message.mentions[0]
                if str(message.mentions[0].id) not in quotes:
                    await message.channel.send("There's no quote for this person!")
                    send = False
                else:
                    quote = quotes[str(message.mentions[0].id)]
            if (send):
                await message.channel.send(auth.display_name + ": " + quote)
        elif (message.content.startswith("!setquote") and config.quotesEnabled):
            msg = message.content.split(" ")
            if (len(msg) == 2):
                num = int(msg[1])
            else:
                num = 1
            time_log("runtime","About to start searching")
            messages = await message.channel.history(limit=20).flatten()
            for i in messages:
                time_log("runtime",i.content)
                if (i.author == message.author):
                    if (num == 0):
                        time_log("runtime",i)
                        addquote(i)
                        time_log("runetime","Done adding!")
                        await message.channel.send("I set " + message.author.display_name +"'s quote to " + i.content)
                    num -= 1
        elif (message.content.startswith("!listquotes") and config.quotesEnabled):
            msg = ""
            for key in quotes:
                mem = message.guild.get_member(int(key))
                if mem is not None:
                    msg += mem.display_name + ": " + quotes[key] + "\n"
            await message.channel.send(msg)




    if communismRegex.search(message.content) and communismEmojiReaction and message.channel.id not in config.communismIgnore:
        time_log("runtime","Found a comrade")
        await message.add_reaction(DDD.get_emoji(config.communismEmojiId))



@DDD.event
async def on_raw_reaction_add(reaction):
    if config.pronounsEnabled and config.pronounRegex.match(str(reaction.emoji)) and reaction.channel_id == config.pronounChannelId:  # If it's a valid
        # pronoun reaction and in the correct channel
        reactionGuild = DDD.get_guild(reaction.guild_id)
        reactionMember = reactionGuild.get_member(reaction.user_id)
        await reactionMember.add_roles(reactionGuild.get_role(config.pronouns[str(reaction.emoji)]))


@DDD.event
async def on_raw_reaction_remove(reaction):
    if config.pronounsEnabled and config.pronounRegex.match(str(reaction.emoji)) and reaction.channel_id == config.pronounChannelId:  # If it's a valid
        # pronoun reaction and in the correct hannel
        reactionGuild = DDD.get_guild(reaction.guild_id)
        reactionMember = reactionGuild.get_member(reaction.user_id)
        await reactionMember.remove_roles(reactionGuild.get_role(config.pronouns[str(reaction.emoji)]))


DDD.loop.create_task(status_update())
DDD.loop.create_task(start_up())
def run(tokenin):
    try:
        DDD.run(tokenin)
    except(discord.LoginFailure):
        with shelve.open("storage.db") as db:
            db["token"] = input("This seems incorrect, please input bot token: ")
        print("Please restart the program") # This shouldn't be neccesary! Recursion should be possible -- but I can't get async to play nice.

run(token)
