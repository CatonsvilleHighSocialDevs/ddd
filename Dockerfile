FROM python:3
RUN pip3 install discord asteval
ADD . /tripled
WORKDIR /tripled
CMD [ "python3", "./Bot.py"]
