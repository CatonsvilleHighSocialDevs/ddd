# Triple D (Document Updated 7/12/2019)

## About

DDD or Dr. Dallas Dance is the mascot for the Catonsville High School's
discord server's bot "Triple D". It is written in Python3.7. This
project is a more formal development compared to the former depreciated
version.

## Triple D's Features

### Commands

#### !owoify \<text>

Triple D will "owoify" text entered in this command. If no text is said
the command, it will use the contents in the prior message.

##### Example Usage:

>!owoify Hello

returns:
> Hewwo

#### !solve \<text>

Triple D safely evaluates the contents in <text> and returns an answer.
It works if !solve \<text> is placed in code blocks as well.

##### Example Usage:
>!solve 2+2

returns:
>4

### Roles

#### Class Roles

Triple D welcomes users and directs them to read the rules of the
server. When they are ready they can select which class they are a part
of without requiring moderation.

#### Pronoun Roles

Catonsville High Social welcomes the LGBTQ+ community as we strive to
create a safe environment for everyone. This is why we have pronoun
roles members can opt in and out very easily by changing their reactions
to a message.

### User Quotes

Triple D keeps a database of "quotes" for users.

#### Setting

You can set a quote for yourself with the commmand
``!setquote``, which sets the last message you sent as your quote. You
can also append a number to it, ``!setquote 2``, and it will use the
your second, third, fourth, ect. to last message.

#### Viewing

The command ``!quote!`` will show your quote. Mentioning a user after
the quote, like ``!quote @Skye`` will show their quote. To see all the
quotes, you can use ``!listquotes``. Note that while quotes are constant
with each Triple D instance, only the quotes of members in the current
server will be shown.

### Channels

#### \#Quotes

Triple D adds upvote/downvote reactions in \#quotes

### Automatic Reactions

Triple D automatically reacts to variations of "our" and "everyone" with
a communism emoji.

## Future Plans

Current future plans for Triple D include day tracking, event reminders,
fun activities, and lower-level management opportunities for other
members.

## Running

Triple D is operated from a private server that automatically checks for
updates that have been pushed, hourly. If there are, the program
automatically restarts with the update.

Soon, formal CI/CD will be implemented.

In addition, a Docker file has been included to facilitate running
Triple D.

Josh and Skye can manually access the server.

## Creation

Triple D's development began on June 20th, 2019 by:

Joshua Noguera '19 (Josh_Player1#4000)

and

Skye Jonke '19 (skyenet#0567)

## Disclaimer

The Triple D project does not support Dr. Dallas Dance or his actions at
all. We actually think he's kind of a disappointment.
