import datetime


def time_log(head, body):
    print("[{} {}]: {}".format(head.upper(), datetime.datetime.now(), body))
