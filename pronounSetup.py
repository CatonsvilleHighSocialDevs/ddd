import config
import discord
import botSecrets
from jLib import time_log
time_log("startup", "Libraries loaded")

DDD = discord.Client()

async def start_up():
    await DDD.wait_until_ready()
    time_log("startup", "Bot is ready")
    msg = await DDD.get_channel(config.pronounChannelId).send(config.message)
    for i in config.pronouns:
        await msg.add_reaction(i)

DDD.loop.create_task(start_up())
DDD.run(botSecrets.client_token)
