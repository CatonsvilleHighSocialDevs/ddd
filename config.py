import re
# Important Channels
devChannel = 591686627240312832
classFlairingChannelId = 503246505973514240
pronounChannelId = 497521597125623840
quotesChannelId = 547413096642117655
announcementsChannelId = 581619992701829144

# Enable Features
quotesChannelEnabled = True
communismEmojiReaction = True
communismIgnore = [499044276949483530] # These are IDs of channels the Communism Emoji should never be places in -- generally serious places where shitty memes shouldn't be.
pronounsEnabled = True
classFlairingEnabled = True
quotesEnabled = True

# Set Features
statusUpdatePeriod = 600  # Period in seconds

# Class Roles
freshmanRoleId = 589274857254944796
sophomoreRoleId = 499209323239440385
juniorRoleId = 377620533799550977
seniorRoleId = 377620454006980616

#Emoji Ids
communismEmojiId = 503341686471000084

# Pronouns
pronouns = {"❤": 508130660645601280, "💙": 508130690907373587, "💜": 508130723341795349 ", "💛": 700146443842879548}  # Dictionary of pronoun emoji unicode
pronounRegex = re.compile("(❤|💙|💜|💛|)")  # Regex to confirm a reaction is a valid pronoun
message = "If you'd like a pronoun role, please react below. :heart: for she/her, :blue_heart: for he/him, and :purple_heart: for they/them, and :yellow_heart: for yo/yo/yos. If your pronouns aren't available, please contact a moderator!"

# Bot Secrets
client_token = "example"
